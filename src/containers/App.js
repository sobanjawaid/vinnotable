import React, { Component } from "react";
import VinnoTable from "../components/VinnoTable/VinnoTable";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableBody: [
        { name: "Jacky", age: 23, cls: "10" },
        { name: "Micky", age: 19, cls: "8" },
        { name: "Harry", age: 25, cls: "6" },
        { name: "Johny", age: 45, cls: "12" }
      ]
    };
  }

  //   componentDidMount() {
  //     this.setState({
  //       tableHead: ["name", "age"],
  //       tableBody: [
  //         { name: "Jacky", age: 23 },
  //         { name: "Micky", age: 19 },
  //         { name: "Harry", age: 25 },
  //         { name: "Johny", age: 45 }
  //       ]
  //     });
  //   }
  render() {
    return (
      <div>
        <VinnoTable tableBody={this.state.tableBody} />
      </div>
    );
  }
}

export default App;
