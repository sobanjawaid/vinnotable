import React, { Component } from "react";

class VinnoTable extends Component {
  constructor(props) {
    super(props);
    console.log("[App.js] constructor");
  }
  render() {
    let renderTableData = () => {
      return this.props.tableBody.map((student, index) => {
        const { name, age, cls } = student;
        return (
          <tr>
            <td>{name}</td>
            <td>{age}</td>
            <td>{cls}</td>
          </tr>
        );
      });
    };

    let renderTableHeader = () => {
      let header = Object.keys(this.props.tableBody[0]);
      console.log(header);
      return header.map((key, index) => {
        return <th key={index}>{key.toUpperCase()}</th>;
      });
    };

    let myData = renderTableData();

    return (
      <div>
          <h1>VinnoTable</h1>
        <div>
          <table id="students">
            <tr>{renderTableHeader()}</tr>
            <tbody>{myData}</tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default VinnoTable;
